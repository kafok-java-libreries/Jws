package com.cubex.jws;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.cubex.jws.app.Code;
import com.cubex.jws.app.Http;
import com.cubex.jws.app.Path;
import com.cubex.jws.http.InterceptorPath;
import com.cubex.jws.http.Request;
import com.cubex.jws.http.StatusCode;
import com.cubex.jws.http.VerbHttp;
import com.cubex.jws.log.JwsEvent;
import com.cubex.jws.log.JwsLogger;
import com.cubex.jws.sessions.MemorySessionManager;
import com.cubex.jws.sessions.SessionManager;

/**
 * Servidor HTTP. Esta clase provee la toda la funcionalidad necesaria para hacer funcionar un servidor HTTP.
 */
public class WebServer implements Runnable {

	private final int port;
	private boolean isRunning;
	private ServerSocket server;
	private InterceptorPath<Map<VerbHttp, Method>> interceptor;
	private Map<Integer, Method> codePages;
	private Method codePage;
	private InterceptorPath<String> rootFolders;
	private SessionManager sessionManager;
	private boolean cacheable;
	private JwsLogger logger;
	private ThreadPoolExecutor executor;
	
	/** @name Constructor
	 */
	///@{
	/**
	 * Constructor a partir de puerto de escucha para el servidor. Este constructor inicializar� todas las configuraciones
	 * del servidor a sus valores por defecto, y deber� asignarle el puerto en el que se ejecutar� el servidor.
	 * 
	 * @param port Puerto en el que se ejecutar� el servidor.
	 */
	public WebServer(int port) {
		this.port = port;
		isRunning = false;
		interceptor = new InterceptorPath<Map<VerbHttp, Method>>();
		codePages = new HashMap<Integer, Method>();
		sessionManager = new MemorySessionManager(1800000);
		rootFolders = new InterceptorPath<String>();
		cacheable = false;
		logger = new JwsLogger();
		executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(4, threadFactory);
	}
	///@}

	
	/** @name Control de flujo
	 */
	///@{
	/**
	 * Empieza a correr el servidor en un nuevo hilo.
	 */
	public void start() {
		isRunning = true;
		sessionManager.start();
		new Thread(this).start();
	}

	/**
	 * Para la ejecuci�n del servidor servidor.
	 */
	public void stop() {
		logger.log("STOPPING", "Stoping server...");
		try {
			isRunning = false;
			if(null != server) {
				server.close();
				server = null;
				sessionManager.stop();
				executor.shutdownNow();
			}
		} catch (IOException e) {
			logger.logException(e);
		}
		
		for(JwsEvent event : logger.getEvents())
			event.onStop();
		logger.log("STOPPED", "Server stoped in port " + port);
	}
	///@}
	

	/** @name Consultores
	 */
	///@{
	/**
	 * Devuelve el puerto donde este servidor se ejecuta, aunque no lo est� en ese momento.
	 * @return Puerto de escucha del servidor.
	 */
	public int getPort() {
		return port;
	}
	
	/**
	 * Devuelve si el servidor est� en funcionamiento, escuchando conexiones por su puerto asignado.
	 * @return - <b>true</b>: Servidor est� corriendo.<br/> - <b>false</b>: El servidor est� parado.
	 */
	public boolean isRunning() {
		return isRunning();
	}
	
	/**
	 * Devuleve el SessionManager de este servidor.
	 * @return SessionManager de este servidor.
	 */
	public SessionManager getSessionManager() {
		return sessionManager;
	}

	/**
	 * Devuelve si el servidor desea que el cliente guarde o no los resultados de sus peticiones en cach�.
	 * @return - <b>true</b>: Servidor proporcionar� al cliente informaci�n necesaria para que pueda cachear los resultados de las peticiones.<br/> - <b>false</b>: El servidor le dir� al cliente que los resultados de sus peticiones no debe guardarlas en cach�.
	 */
	public boolean isCacheable() {
		return cacheable;
	}
	
	/**
	 * Devuelve el JwsLogger asociado a este servidor.
	 * @return JwsLogger asociado a este servidor.
	 */
	public JwsLogger getLogger() {
		return logger;
	}
	
	/**
	 * Devuelve el ExecutorService asociado a este servidor.
	 * @return ExecutorService asociado a este servidor.
	 */
	public ExecutorService getExecutor() {
		return executor;
	}
	///@}


	/** @name Establecer configuraci�n
	 */
	///@{
	/**
	 * Establece el SessionManager para este servidor. Por defecto el servido tendr� un MemorySessionManager con un TTL de media hora.
	 * @param sessionManager SessionManager para este servidor.
	 */
	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}
	
	/**
	 * Establece si el servidor quiere que el cliente guarde en su cach� los resultados de peticiones a este. Por defecto es <b>false</b>.
	 * @param cacheable - <b>true</b>: Servidor proporcionar� al cliente informaci�n necesaria para que pueda cachear los resultados de las peticiones.<br/> - <b>false</b>: El servidor le dir� al cliente que los resultados de sus peticiones no debe guardarlas en cach�.
	 */
	public void setCacheable(boolean cacheable) {
		this.cacheable = cacheable;
	}
	
	/**
	 * Establece el JwsLogger de este servidor. Por defecto es un JwsLogger por defecto.
	 * @param logger JwsLogger de este servidor.
	 */
	public void setLogger(JwsLogger logger) {
		this.logger = logger;
	}
	
	/**
	 * Establece el ThreadPoolExecutor de este servidor. Por defecto tendr� un pool de 10 hilos (n�mero de hilos fijos).
	 * @param executor ThreadPoolExecutor de este servidor.
	 */
	public void setExecutor(ThreadPoolExecutor executor) {
		executor.setThreadFactory(threadFactory);
		this.executor = executor;
	}
	///@}


	/** @name Aplicaci�n
	 */
	///@{
	/**
	 * Agrega una clase controlador a este servidor a partir de la ruta path.
	 * @param path Ruta a partir de la que se a�ade la clase controller a este servidor.
	 * @param controller Clase controlador que se deseea a�adir a este servidor.
	 */
	public void addController(String path, Class<?> controller) {
		// Set path
		path = path.endsWith("/") ? path.substring(0, path.length()-1) : path;
		Path apath = controller.getAnnotation(Path.class);
		if(apath != null)
			path += "/" + apath.value();
		
		// Scan method
		for(Method method : controller.getDeclaredMethods()) {
			Code code = method.getAnnotation(Code.class);
			apath = method.getAnnotation(Path.class);
			
			// Filter: Valid methods
			if(apath == null && code == null)
				continue;
			else {
				if(!Modifier.isStatic(method.getModifiers())) {
					logger.logWarning("Method \"" + method.toString() + "\" must be static!");
					continue;
				}
				
				if(!Modifier.isPublic(method.getModifiers())) {
					logger.logWarning("Method \"" + method.toString() + "\" must be public!");
					continue;
				}
			}
			
			// Path
			if(apath != null) {
				String fpath = path.endsWith("/") ? path.substring(0, path.length()-1) : path;
				String _path = apath.value();
				String spath = _path.startsWith("/") ? _path.substring(1, _path.length()) : _path;
				_path = fpath + "/" + spath;
				
				Map<VerbHttp, Method> map = interceptor.fromPath(_path);
				if(map == null)
					map = new HashMap<VerbHttp, Method>();
				
				if(map.containsKey(apath.verb()))
					logger.logWarning("Path \"" + apath.value() + "\" already exists with verb " + apath.verb());
				
				map.put(apath.verb(), method);
				
				_path = _path.replace("//", "/");
				interceptor.addPath(_path, map);
				logger.log("LOADED", "Loaded controller in path \"" + _path + "\" with verb " + apath.verb());
			}
			
			// Code
			if(code != null) {
				if(code.value() < 0) {
					if(codePage != null)
						logger.logWarning("Generic code already in use by method \"" + codePage.toString() + "\"");
					
					codePage = method;
					logger.log("LOADED", "Loaded controller for status codes");
				} else {
					Method mCode = codePages.get(code.value());
					if(mCode != null)
						logger.logWarning("Code " + code.value() + " already in use by method \"" + mCode.toString() + "\"");
						
					codePages.put(code.value(), method);
					logger.log("LOADED", "Loaded controller for status code " + code.value());
				}
			}
		}
	}
	
	/**
	 * Agrega una ruta del sistema como ruta de archivos a una ruta de este servidor. A partir de la ruta path se acceder�n a los archivos
	 * guardados en la ruta del sistema root y se devolver�n dichos archivos tal cual.
	 * @param root Ruta del sistema de la que a partir de ella sus archivos ser�n visibles para el servidor.
	 * @param path Ruta a la que se acceder� en el servidor, desde la cual se pedir�n los archivos que se encuentran en a partir de root.
	 */
	public void addFileController(String root, String path) {
		path = path.endsWith("/") ? path.substring(0, path.length()-1) : path;
		try {
			root = new File(root).getCanonicalPath();
			rootFolders.addPath(path + "/*", root);
			logger.log("LOADED", "Loaded file controller with root \"" + root + "\"");
		} catch (IOException e) {
			logger.logException(e);
		}
	}
	
	/**
	 * Agrega una clase controlador a este servidor.
	 * @param controller Clase controlador que se deseea a�adir.
	 */
	public void addController(Class<?> controller) {
		addController("", controller);
	}
	
	/**
	 * Escanea los controladores del paquete packageName y los agrega a partir de la ruta path.
	 * @param path Ruta a partir de la cual se establecer�n los controladores.
	 * @param packageName Paquete que se ecanear� recursivamente para encontrar las clases controladores, las
	 * cuales se agregar�n a este servidor.
	 */
	public void addControllers(String path, String packageName) {
		for(Class<?> clazz : getClasses(packageName))
				addController(path, clazz);
	}
	
	/**
	 * Escanea los controladores del paquete packageName.
	 * @param packageName Paquete que se ecanear� recursivamente para encontrar las clases controladores, las
	 * cuales se agregar�n a este servidor.
	 */
	public void addControllers(String packageName) {
		addControllers("", packageName);
	}
	///@}
	
	
	/** @name Run
	 */
	///@{
	/**
	 * Empieza a ejecutar el servidor en el hilo que llame a este m�todo. La ejecuci�n acabar� cuando se llame al m�todo stop() � salte alguna excepci�n
	 * grave, como que el puerto est� en uso.
	 */
	@Override
	public void run() {
		try {
			logger.log("STARTING", "Starting server...");
			server = new ServerSocket(port);
			logger.log("STARTED", "Server started in port " + port);
			
			for(JwsEvent event : logger.getEvents())
				event.onStart();
			
			while(isRunning) {
				Socket socket = server.accept();
				HandleRequest handle = new HandleRequest(socket);
				executor.execute(handle);
			}
		} catch (SocketException e) {
			logger.logException(e);
		} catch (IOException e) {
			logger.logException(e);
		}
		sessionManager.stop();
	}
	///@}


	private boolean writeWithCode(PrintStream output, int code, String ip, Http http) {
		try {
			// Events
			logger.log("RESPONSE", code + " " + StatusCode.valueOf(code).toHumanString() + " from " + ip);
			for(JwsEvent event : logger.getEvents())
				event.onResponse(ip, http);
			
			byte[] bytes;
			Method method = codePages.get(code);
			if(method == null)
				bytes = codePage.invoke(null, code).toString().getBytes();
			else
				bytes = method.invoke(null).toString().getBytes();
			
			output.println("HTTP/1.1 " + code + " " + StatusCode.valueOf(code).toHumanString());
			
			output.println("Content-Length: " + bytes.length);
			output.println();
			if(bytes != null)
				output.write(bytes);
			output.flush();
		} catch(Throwable e) {
			return false;
		}
		
		return true;
	}
	
	private void writeServerError(PrintStream output, String ip, Http http) {
		if(writeWithCode(output, 500, ip, http))
			return;
		
		output.println("HTTP/1.1 500 Internal Server Error");
		output.flush();
	}
	
	private void writeResponse(PrintStream output, Http http, Object bytes, String ip) throws IOException {
		// Events
		int code = http.getResponse().getStatusCode();
		logger.log("RESPONSE", code + " " + StatusCode.valueOf(code).toHumanString() + " from " + ip);
		for(JwsEvent event : logger.getEvents())
			event.onResponse(ip, http);
		
		output.println("HTTP/1.1 " + code + " " + StatusCode.valueOf(code).toHumanString());
		
		for(Map.Entry<String, String> header : http.getResponse().getHeaders().entrySet())
			output.println(header.getKey() + ": " + header.getValue());
		
		for(String cookie : http.getResponseCookies())
			output.println("Set-Cookie: " + cookie);
		
		String schunked = http.getResponse().getHeaders().get("Transfer-Encoding");
		boolean chunked = schunked != null && schunked.equals("chunked");
		if(bytes != null && !chunked)
			output.println("Content-Length: " + ((byte[])bytes).length);
		
		output.println();
		if(bytes != null) {
			if(chunked)
				chunked((InputStream) bytes, output);
			else
				output.write((byte[]) bytes);
		}

		output.flush();
	}
	
	private void writeClientError(PrintStream output, int code, String msg, String content, String ip, Http http) throws IOException {
		if(writeWithCode(output, code, ip, http))
			return;
		
		byte[] bytes = null;
		if(content != null)
			bytes = content.getBytes();
		
		output.println("HTTP/1.1 " + code + " " + msg);
		if(content != null) {
			output.println("Content-Type: text/html");
			output.println("Content-Length: " + bytes.length);
			output.println();
			output.write(bytes);
		} else
			output.println();
		
		output.flush();
	}
	
	private List<Class<?>> getClasses(String packageName) {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		URL packageURL;
		JarFile jf = null;
		List<Class<?>> clazzes = new LinkedList<Class<?>>();
		packageName = packageName.replace(".", "/");
		packageURL = loader.getResource(packageName);

		try {
			if(packageURL.getProtocol().equals("jar")) {
				String jarFileName;
				Enumeration<JarEntry> jarEntries;
	
				// build jar file name, then loop through zipped entries
				jarFileName = URLDecoder.decode(packageURL.getFile(), "UTF-8");
				jarFileName = jarFileName.substring(5, jarFileName.indexOf("!"));
				System.out.println(">" + jarFileName);
				jf = new JarFile(jarFileName);
				jarEntries = jf.entries();
				while(jarEntries.hasMoreElements()) {
					JarEntry entry = jarEntries.nextElement();
					if(entry.getName().startsWith(packageName) && !entry.isDirectory() && entry.getName().endsWith(".class"))
						clazzes.add(loader.loadClass(entry.getName().substring(0, entry.getName().length() - 6).replace("/", ".")));
				}
	
				// loop through files in classpath
			} else {
				URI uri = new URI(packageURL.toString());
				File folder = new File(uri.getPath());
				File[] files = folder.listFiles();
				for(File file : files) {
					if(file.isDirectory())
						clazzes.addAll(getClasses(packageName + "." + file.getName()));
					else if(file.getName().endsWith(".class"))
						clazzes.add(loader.loadClass(packageName.replace("/", ".") + "." + file.getName().substring(0, file.getName().length() - 6)));
				}
			}
		} catch(Throwable e) {
			logger.logException(e);
		} finally {
			if(jf != null)
				try {
					jf.close();
				} catch (IOException e) {
					logger.logException(e);
				}
		}
		
		return clazzes;
	}
	
	private static File fileController(Http http, String url, String root) throws IOException {
		File file = new File(root + "/" + url).getCanonicalFile();
		if(!file.getAbsolutePath().startsWith(root)) {
			http.getResponse().setStatusCode(403);
			return null;
		}

		Calendar modificated = Calendar.getInstance();
		String last;
		if(http.getServer().isCacheable()) {
			// GET Conditional
			String match = http.getRequest().getHeaders().get("If-None-Match");
			if(match != null && match.equals("3-"+file.lastModified())) {
				http.getResponse().setStatusCode(304);
				return null;
			}
			
			String since = http.getRequest().getHeaders().get("If-Modified-Since");
			modificated.setTimeInMillis(file.lastModified());
			last = Http.getHttpDate(modificated);
			if(since != null && since.equals(last)) {
				http.getResponse().setStatusCode(304);
				return null;
			}
			
			//ETag
			http.getResponse().getHeaders().put("ETag", "3-"+file.lastModified());
		} else {
			http.getResponse().getHeaders().put("Cache-Control", "no-cache, no-store, must-revalidate");
			last = Http.getHttpDate(modificated);
		}
		
		// Last-Modified
		http.getResponse().getHeaders().put("Last-Modified", last);
		
		return http.getRequest().getVerb().equals(VerbHttp.HEAD) ? null : file;
	}
	
	private void chunked(InputStream input, OutputStream output) throws IOException {
		int num;
		byte[] data = new byte[65535];
		byte[] end = "\r\n".getBytes();
		
		while ((num = input.read(data, 0, data.length)) != -1) {
			output.write((Integer.toHexString(num).toUpperCase() + "\r\n").getBytes());
			output.write(data, 0, num);
			output.write(end);
		}

		output.write("0\r\n\r\n".getBytes());
		
		input.close();
	}
	

	private class HandleRequest implements Runnable {

		private Socket socket;

		public HandleRequest(Socket socket) {
			super();
			this.socket = socket;
			try {
				socket.setSoTimeout(300);
			} catch (SocketException e) {
				logger.logException(e);
			}
		}


		@Override
		public void run() {
			try {
				while(!socket.isClosed())
					handle();
			} catch (Throwable e) {
				logger.logException(e);
			}
		}

		private void handle() throws IOException {
			PrintStream output = null;
			boolean closed = false;
			try {
				String ip = socket.getRemoteSocketAddress().toString().substring(1);
				
				// Output
				output = new PrintStream(socket.getOutputStream());
				
				// Read request
				Request request;
				try {
					DataInputStream reader = null;
					reader = new DataInputStream(socket.getInputStream());
					request = Request.parse(reader);
					
					// Keep-Alive
					String conection = request.getHeaders().get("Connection");
					boolean conect = conection != null && conection.equals("keep-alive");
					if(conect) {
						try {
							socket.setKeepAlive(true);
						} catch (SocketException e) {
							logger.logException(e);
						}
					}
					
					// Events
					StringBuilder sb = new StringBuilder();
					Map<String, String> params = new HashMap<String, String>();
					Map<VerbHttp, Method> map = interceptor.fromPath(request.getPath(), params);
					String root = null;
					if(map == null)
						root = rootFolders.fromPath(request.getPath(), sb);
					
					// Events
					logger.log("REQUEST", request.getVerb() + " " + request.getPath() + " from " + socket.getRemoteSocketAddress().toString().substring(1));
					for(JwsEvent event : logger.getEvents())
						event.onRequest(ip, request, null);
					
					if(map == null && root == null) {
						writeClientError(output, 404, "Not found", null, ip, new Http(request, WebServer.this));
					} else {
						Method method = null;
						if(root == null) {
							method = map.get(VerbHttp.valueOf(request.getVerb()));
							if(method == null) {
								writeClientError(output, 404, "Not found", null, ip, new Http(request, WebServer.this));
								return;
							}
						}
						
						// Request content
						Http http;
						String contentType = request.getHeaders().get("Content-Type"); 
						if(contentType != null)
							http = Request.parseContent(reader, contentType, request, WebServer.this);
						else
							http = new Http(request, WebServer.this);
						
						Object res;
						if(root == null) {	// If is a script
							Class<?>[] par = method.getParameterTypes();
							Object values[] = new Object[par.length];
							
							// Params
							values[0] = http;
							for(int i=1; i<par.length; i++) {
								String val = params.get(""+i);
								val = URLDecoder.decode(val, "UTF-8");
								values[i] = objectToString(val, primitiveToWarpper(par[i]));
							}
							
							// Script
							res = method.invoke(null, values);
						} else {	//If is a resource
							res = fileController(http, sb.toString(), root);
						}
						
						// Return Types
						Object response = null;
						if(res != null) {
							String schunked = http.getResponse().getHeaders().get("Transfer-Encoding");
							boolean chunked = schunked != null && schunked.equals("chunked");
							if(res instanceof InputStream) {
								if(chunked)
									response = res;
								else
									response = toByte((InputStream) res);
							} else if(res instanceof File) {
								File file = (File)res;
								
								Closeable input = null;
								try {
									if(chunked)
										response = new FileInputStream(file);
									else {
										Http.Range range = http.getRange();
										if(!range.isComplete()) {
											if(!range.isUntilEnd() && range.getEnd() >= file.length()) {
												http.getResponse().setStatusCode(416);
												response = null;
											} else  {
												String match = http.getRequest().getHeaders().get("If-Range");
												boolean cached;
												if(match != null) {
													if( match.startsWith("3-"))
														cached = match.equals("3-"+file.lastModified());
													else {
														Calendar c = Calendar.getInstance();
														c.setTimeInMillis(file.lastModified());
														cached = match.equals(Http.getHttpDate(c));
													}
												} else
													cached = false;
												
												if(!cached) {
													input = new RandomAccessFile(file, "r");
													long end = range.isUntilEnd() ? file.length()-1 : range.getEnd();
													response = toByte(range.getInit(), end, (RandomAccessFile) input);
													http.getResponse().setStatusCode(206);
													http.getResponse().getHeaders().put("Content-Range", "bytes " + range.getInit() + "-" + end + "/" + file.length());
												} else {
													input = new FileInputStream(file);
													response = toByte(new FileInputStream(file));
												}
											}
										} else {
											input = new FileInputStream(file);
											response = toByte(new FileInputStream(file));
										}
									}
								} catch(FileNotFoundException e) {
									writeClientError(output, 404, "Not found", null, ip, http);
									return;
								} finally {
									if(input != null && !chunked)
										input.close();
								}
								
								if(!http.getResponse().getHeaders().containsKey("Content-Type"))
									http.getResponse().getHeaders().put("Content-Type", MimeType.getMimeType(getExtension(file.getName())));
							} else {
								Path aPath = method.getAnnotation(Path.class);
								if(chunked)
									response = new ByteArrayInputStream(((String) res).getBytes(aPath.charset()));
								else
									response = res.toString().getBytes(aPath.charset());
								
								if(!http.getResponse().getHeaders().containsKey("Content-Type"))
									http.getResponse().getHeaders().put("Content-Type", MimeType.getMimeType(aPath.format()) + "; charset=" + aPath.charset());
							}
							
							http.getResponse().getHeaders().put("Connection", conect ? conection : "close");	// Keep-Alive
							http.getResponse().getHeaders().put("Accept-Ranges", "bytes");						// Accept-Ranges
							writeResponse(output, http, response, ip);
						} else {
							// Events
							for(JwsEvent event : logger.getEvents())
								event.onResponse(socket.getRemoteSocketAddress().toString().substring(1), http);
							
							int code = http.getResponse().getStatusCode();
							if((code >= 300 && code < 400) || http.getRequest().getVerb().equals(VerbHttp.HEAD))
								writeResponse(output, http, response, ip);
							else if(!writeWithCode(output, code, ip, http)) {
								writeServerError(output, ip, http);
								closed = true;
							}
						}
					}
				} catch(IllegalArgumentException e) {
					writeClientError(output, 400, "Bad request", "Bad request", ip, new Http(null, WebServer.this));
					return;
				} catch(SocketTimeoutException | SocketException e) {
					try {
						socket.setKeepAlive(false);
					} catch (SocketException oops) {
						logger.logException(oops);
					}
					
					if (null != output)
						output.close();
					
					socket.close();
				} catch(Throwable e) {
					if(e instanceof InvocationTargetException)
						e = ((InvocationTargetException)e).getTargetException();
					
					logger.logException(e);
					writeServerError(output, ip, new Http(null, WebServer.this));
					closed = true;
				}
				
			} catch(Throwable e) {
				logger.logException(e);
			} finally {
				if(closed) {
					try {
						socket.setKeepAlive(false);
					} catch (SocketException oops) {
						logger.logException(oops);
					}
					
					if (null != output)
						output.close();
					
					socket.close();
					return;
				}
				
				if(!socket.isClosed() && !socket.getKeepAlive()) {
					if (null != output)
						output.close();
					
					socket.close();
				}
			}
		}
		
		private Object objectToString(String s, Class<?> clazz) {
			if(s == null)
				return null;
			
			try {
				return clazz.getConstructor(new Class[] {String.class }).newInstance(s);
			} catch(Exception e) {
				return null;
			}
		}
		
		private Class<?> primitiveToWarpper(Class<?> primitive) {
			
			if(!primitive.isPrimitive())
				return primitive;
			
			if(primitive.equals(boolean.class))
				return Boolean.class;
			else if(primitive.equals(byte.class)) 
				return Byte.class;
			else if(primitive.equals(char.class)) 
				return Character.class;
			else if(primitive.equals(double.class)) 
				return Double.class;
			else if(primitive.equals(float.class)) 
				return Float.class;
			else if(primitive.equals(int.class)) 
				return Integer.class;
			else if(primitive.equals(long.class)) 
				return Long.class;
			else if(primitive.equals(short.class)) 
				return Short.class;
			else if(primitive.equals(void.class)) 
				return Void.class;
			
			return null;
		}
		
		private byte[] toByte(InputStream input) throws IOException {
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();

			int num;
			byte[] data = new byte[4096];

			while ((num = input.read(data, 0, data.length)) != -1)
				buffer.write(data, 0, num);

			buffer.flush();

			return buffer.toByteArray();
		}
		
		private byte[] toByte(long a, long b, RandomAccessFile input) throws IOException {
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();

			int num;
			byte[] data = new byte[4096];
			long count = a;

			input.seek(a);
			while(count < b && (num = input.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, num);
				count += num;
			}

			buffer.flush();

			return buffer.toByteArray();
		}
		
		private String getExtension(String file) {
			String extension = "";

			int i = file.lastIndexOf('.');
			if (i > 0)
				extension = file.substring(i + 1);
			
			return extension;
		}
	}
	
	
	private static WebThreadFactory threadFactory = new WebThreadFactory();
	
	private static class WebThreadFactory implements ThreadFactory {

		public Thread newThread(Runnable r) {
			Thread thread = new Thread(r);
			thread.setName("HTTP Server");
			return thread;
		}
		
	}
}

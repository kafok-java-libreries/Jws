package com.cubex.jws.app;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Code {
	
	int value() default -1;	//TODO Doc, valores negativos son todos los c�digos, cuando no existe un c�digo ya.
}

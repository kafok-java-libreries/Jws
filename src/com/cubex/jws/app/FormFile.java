package com.cubex.jws.app;

public class FormFile {
	
	private String name;
	private String mimeType;
	private byte[] file;
	
	public FormFile(String name, String mimeType, byte[] file) {
		super();
		this.name = name;
		this.mimeType = mimeType;
		this.file = file;
	}

	public String getName() {
		return name;
	}

	public String getMimeType() {
		return mimeType;
	}

	public byte[] getFile() {
		return file;
	}
}

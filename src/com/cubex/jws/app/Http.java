package com.cubex.jws.app;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.cubex.jws.WebServer;
import com.cubex.jws.http.Request;
import com.cubex.jws.http.Response;
import com.cubex.jws.sessions.Session;
import com.cubex.jws.sessions.SessionManager;

public class Http {
	
	private Request request;
	private Response response;
	
	private Map<String, Object> forms;
	private Object form;
	private Map<String, String> cookie;
	private WebServer server;
	private List<String> responseCookies;
	private Range range;
	
	
	public Http(Request request, WebServer server) {
		super();
		this.request = request;
		this.server = server;
		response = new Response();
		forms = new HashMap<String, Object>();
		form = forms;
		cookie = new HashMap<String, String>();
		responseCookies = new LinkedList<String>();
		
		
		if(request != null) {
			// Read cookie
			readCookies(request);
			
			// Read range
			range = new Range(request.getHeaders().get("Range"));
		} else
			range = new Range(null);
		
	}
	
	public Http(Request request, Map<String, Object> forms, WebServer server) {
		super();
		this.request = request;
		this.server = server;
		this.forms = forms;
		form = forms;
		response = new Response();
		cookie = new HashMap<String, String>();
		responseCookies = new LinkedList<String>();
		
		if(request != null) {
			// Read cookie
			readCookies(request);
			
			// Read range
			range = new Range(request.getHeaders().get("Range"));
		} else
			range = new Range(null);
	}
	
	public Http(Request request, Object form, WebServer server) {
		super();
		this.request = request;
		this.server = server;
		this.form = form;
		response = new Response();
		forms = new HashMap<String, Object>();
		cookie = new HashMap<String, String>();
		responseCookies = new LinkedList<String>();
		
		if(request != null) {
			// Read cookie
			readCookies(request);
			
			// Read range
			range = new Range(request.getHeaders().get("Range"));
		} else
			range = new Range(null);
	}


	private void readCookies(Request request) {
		String headerCookies = request.getHeaders().get("Cookie");
		if(headerCookies != null) {
			String cookies[] = headerCookies.split(";");
			for(int i=0; i<cookies.length; i++) {
				int index = cookies[i].indexOf('=');
				if(index < 0)
					continue;
				
				cookie.put(cookies[i].substring(0, index).trim(), cookies[i].substring(index+1, cookies[i].length()).trim());
			}
		}
	}


	public Request getRequest() {
		return request;
	}

	public Response getResponse() {
		return response;
	}

	public String get(String get) {
		return request.getQueries().get(get);
	}

	public Object form() {
		return this.form;
	}
	
	public Object form(String form) {
		return this.forms.get(form);
	}

	public String cookie(String cookie) {
		return this.cookie.get(cookie);
	}
	
	public Object session(String obj) {
		SessionManager manager = server.getSessionManager();
		String id = cookie(manager.getName());
		
		if(id != null) {
			Session session = manager.find(id);
			if(session != null)
				return session.get(obj);
			else
				createSession(manager);
		} else
			createSession(manager);
		
		return null;
	}
	
	public List<String> getResponseCookies() {
		return responseCookies;
	}
	
	
	public void setCookie(String name, String value, int expire, String... attributes) {//TODO DOC: https://en.wikipedia.org/wiki/HTTP_cookie
		String res;
		if (expire > 0) {
			Calendar exp = Calendar.getInstance();
			exp.setTimeInMillis(Calendar.getInstance().getTimeInMillis() + (long) (expire * 1000));

			res = "; Expires=" + getHttpDate(exp);
			res += ("; Max-Age=" + expire);
		} else if (expire == 0)
			res = "";
		else { // TODO DOC: numero negativo elimina cookie, cero es cookie de session, positivo duracion en segundos
			Calendar exp = Calendar.getInstance();
			exp.setTimeInMillis(0);

			res = "; Expires=" + getHttpDate(exp);
			res += ("; Max-Age=" + (exp.getTimeInMillis() - Calendar.getInstance().getTimeInMillis()));
		}

		// atributos
		String cookie = name + "=" + value + res;
		for (String a : attributes)
			cookie += ("; " + a);

		// establece en cabecera
		responseCookies.add(cookie);
	}

	public static String getHttpDate(Calendar c) {
		String res = "";

		DateFormat dow = new SimpleDateFormat("EEE", Locale.US);
		DateFormat fecha = new SimpleDateFormat("dd-MMM-yyy", Locale.US);
		DateFormat hora = new SimpleDateFormat("HH:mm:ss", Locale.US);
		try {
			String _dow = dow.format(c.getTime());
			String _fecha = fecha.format(c.getTime());
			String _hora = hora.format(c.getTime());

			_dow = Character.toUpperCase(_dow.charAt(0)) + _dow.substring(1, 3);
			_fecha = _fecha.substring(0, 3) + Character.toUpperCase(_fecha.charAt(3)) + _fecha.substring(4);

			res += (_dow + ", " + _fecha + " " + _hora + " GMT");
		} catch (Exception e1) {
		}

		return res;
	}
	
	
	public void setSession(String obj, Object value) {
		SessionManager manager = server.getSessionManager();
		String id = cookie(manager.getName());
		
		Session session;
		if(id != null) {
			session = manager.find(id);
			if(session != null) {
				session.put(obj, value);
				return;
			} else
				session = createSession(manager);
		} else
			session = createSession(manager);
		
		session.put(obj, value);
	}


	private Session createSession(SessionManager manager) {
		Session session = manager.create();
		cookie.put(manager.getName(), session.getId());
		setCookie(manager.getName(), session.getId(), 0, "HttpOnly");
		return session;
	}
	
	
	public void setDownloadable(String name) {	//TODO DOC No funciona si se desactiva la cache
		getResponse().getHeaders().put("Content-Disposition", "attachment; filename=\"" + name + "\"");
		getResponse().getHeaders().put("Content-Type", "application/octet-stream");
	}
	
	public void redirect(String location) {
		getResponse().setStatusCode(302);
		getResponse().getHeaders().put("Location", location);
	}
	
	public WebServer getServer() {
		return server;
	}
	
	public Range getRange() {
		return range;
	}

	public void setStreaming() {
		getResponse().getHeaders().put("Transfer-Encoding", "chunked");
	}


	public static class Range {
		
		private long init;
		private long end;
		
		
		private Range(String value) {
			super();
			if(value != null && !value.isEmpty()) {
				int index = value.indexOf(',');
				if(index < 0)
					index = value.length();
				
				if(!value.startsWith("bytes="))
					throwParseError();
				
				String range = value.substring(6, index);
				String values[] = range.split("[-]");
				if(values.length != 2 && values.length != 1)
					throwParseError();
				
				init = new Long(values[0]);
				
				if(init < 0)
					throwParseError();
				
				end = values.length == 2 && !values[1].isEmpty() ? new Long(values[1]) : -1;
				
			} else {
				init = 0;
				end = -1;
			}
		}
		

		public long getInit() {
			return init;
		}
		
		public long getEnd() {
			return end;
		}
		
		public boolean isComplete() {
			return init == 0 && end < 0;
		}
		
		public boolean isUntilEnd() {
			return end < 0;
		}
		
		
		private static void throwParseError() {
			throw new IllegalArgumentException("Bad request!");
		}
		
		
		public String toString() {
			return "[" + init + ", " + end + "]";
		}
	}
}

package com.cubex.jws.app;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.cubex.jws.http.VerbHttp;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Path {
	
	VerbHttp verb() default VerbHttp.GET;
	String value();
	String format() default "html";
	String charset() default "utf-8";
}

package com.cubex.jws.http;

import java.util.HashMap;
import java.util.Map;

/**
 * Esta clase representa un �rbol de rutas, es decir, un �rbol con valores T en los nodos ramas y que
 * cada nodo (intermedio o rama) est� representado por una cadena. De esta forma para acceder al valor
 * de una rama, habr� que utilizar su ruta, que se compone del valor textual de cada nodo concatenado
 * por "/", por ejemplo: /xxx/yyy/zzz
 * 
 * El funcionamiente es el siguiente, se pueden tanto a�adir valores a una ruta, como recurear el valor
 * asociado a una ruta dada.
 *
 * @param <T> Tipo del valor que se le asignar� a cada rama del �rbol.
 */
public class InterceptorPath<T> {

	private Map<String, Node<T>> childs;
	private T root;
	
	/**
	 * Constructor por defecto. Crea un InterceptorPath vaci�.
	 */
	public InterceptorPath() {
		super();
		this.childs = new HashMap<String, Node<T>>();
	}
	
	/**
	 * A�ade un valor a una ruta. Si la ruta ya ten�a un valor, se sobrescribe. Los valores solo pueden
	 * ser a�adidos al final de una ruta, si una ruta intermedia ten�a un valor, este se borrar�.
	 * 
	 * @param path Ruta donde se encuentrar� el nuevo valor.
	 * @param handle Valor almacenado en la ruta dada.
	 */
	public void addPath(String path, T handle) {
		if(handle == null)
			throw new IllegalArgumentException("The handle can not be null");
		
		path = path.startsWith("/") ? path.substring(1, path.length()) : path;
		
		boolean infinite = false;
		if(path.endsWith("/*")) {
			path = path.substring(0, path.length() - 2);
			infinite = true;
		}
		
		String[] route = path.split("[/]");
		
		if(route.length == 0 || (route.length == 1 && route[0].isEmpty())) {
			root = handle;
			return;
		}
		
		int i = route[0].isEmpty() ? 1 : 0;
		
		Node<T> child = childs.get(route[i]);
		if(child == null) {
			child = new Node<T>();
			put(route[i], child);
		}
		
		if(route.length <= i+1) 
			put(route[i], new NodeBranch<T>(handle, infinite));
		else
			addPath(i+1, null, child, route, handle, infinite);
	}
	
	private void addPath(int i, Node<T> parent, Node<T> child, String[] route, T handle, boolean infinite) {
		if(route.length <= i+1) {
			if(child.isBranch()) {
				child = new Node<T>();
				if(parent != null)
					put(parent.getChilds(), route[i-1], child);
				else
					put(route[i-1], child);
			}
			
			put(child.getChilds(), route[i], new NodeBranch<T>(handle, infinite));
		} else {
			if(child.isBranch())
				throw new IllegalStateException("Path can not contain more children");
				
			Node<T> node = child.getChilds().get(route[i]);
			if(node == null) {
				node = new Node<T>();
				put(child.getChilds(), route[i], node);
			}
			
			addPath(i+1, child, node, route, handle, infinite);
		}
	}
	
	private void put(String route, Node<T> node) {
		put(childs, route, node);
	}
	
	private void put(Map<String, Node<T>> childs, String route, Node<T> node) {
		if(route.startsWith("{") && route.endsWith("}") && route.length() >= 3) {	//TODO DOC Documentar esta sintaxis
			childs.put("{#}", node);
			node.setParameter(route.substring(1, route.length()-1));
		} else
			childs.put(route, node);
	}
	
	
	/**
	 * Recupera un valor dada una ruta. Devuelve nulo si no lo encuentra o si es una ruta intermedia.
	 * 
	 * @param path Ruta donde buscar.
	 * @param params Lista donde se guardar�n el valor de los par�metros.
	 * @return Valor almacenado en la ruta dada. Nulo si no existe dicha ruta.
	 */
	public T fromPath(String path, Map<String, String> params, StringBuilder param) {
		path = path.startsWith("/") ? path.substring(1, path.length()) : path;
		path = path.endsWith("/") ? path.substring(0, path.length()-1) : path;
		String[] route = path.split("[/]");
		StringBuilder temp = new StringBuilder();
		
		if(route.length == 0 || (route.length == 1 && route[0].isEmpty()))
			return root;
		
		int i = route[0].isEmpty() ? 1 : 0;
		Node<T> node = childs.get(route[i]);
		temp.append(route[i]);
		i++;
		while(node != null && route.length > i && !node.isBranch()) {
			Node<T> _node = node.getChilds().get(route[i]);
			if(_node == null) {
				_node = node.getChilds().get("{#}");
				if(_node != null && params != null)
					params.put(_node.getParameter(), route[i]);
			}
			
			node = _node;
			temp.append("/");
			temp.append(route[i]);
			i++;
		}
		
		if(node == null)
			return null;
		
		String sTemp = temp.toString();
		if(!node.isInfinite() && !sTemp.equals(path))
			return null;
		
		if(param != null) {
			param.setLength(0);
			param.append(path.substring(sTemp.length()+1, path.length()));
		}
		
		return node.getHandle();
	}
	
	/**
	 * Recupera un valor dada una ruta. Devuelve nulo si no lo encuentra o si es una ruta intermedia.
	 * 
	 * @param path Ruta donde buscar.
	 * @return Valor almacenado en la ruta dada. Nulo si no existe dicha ruta.
	 */
	public T fromPath(String path) {
		return fromPath(path, null, null);
	}
	
	public T fromPath(String path, StringBuilder param) {
		return fromPath(path, null, param);
	}
	
	public T fromPath(String path, Map<String, String> params) {
		return fromPath(path, params, null);
	}
	
	

	private static class Node<P> {
		
		protected Map<String, Node<P>> childs;
		private String parameter;
		
		public Node() {
			super();
			this.childs = new HashMap<String, Node<P>>();
		}
		
		public Map<String, Node<P>> getChilds() {
			return childs;
		}
		
		public P getHandle() {
			return null;
		}
		
		public String getParameter() {
			return parameter;
		}
		
		public void setParameter(String parameter) {
			this.parameter = parameter;
		}

		public boolean isBranch() {
			return false;
		}
		
		public boolean isInfinite() {
			return false;
		}
		
	}
	
	private static class NodeBranch<P> extends Node<P> {
		
		private P handle;
		private boolean isInfinite;
		
		public NodeBranch(P handle, boolean isInfinite) {
			super();
			this.childs = null;
			this.handle = handle;
			this.isInfinite = isInfinite;
		}

		public P getHandle() {
			return handle;
		}
		
		public boolean isBranch() {
			return true;
		}
		
		public boolean isInfinite() {
			return isInfinite;
		}
	}
	
}

package com.cubex.jws.http;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import com.cubex.jws.WebServer;
import com.cubex.jws.app.FormFile;
import com.cubex.jws.app.Http;

public class Request {
	
	private String verb;
	private String path;
	private Map<String, String> queries;
	private Map<String, String> headers;
	

	public Request(String verb, String path, Map<String, String> queries, Map<String, String> headers) {
		super();
		this.verb = verb;
		this.path = path;
		this.queries = queries;
		this.headers = headers;
	}
	
	public Request(String verb, String path) {
		super();
		this.verb = verb;
		this.path = path;
		this.queries = new HashMap<String, String>();
		this.headers = new HeaderMap();
	}
	
	
	public String getVerb() {
		return verb;
	}

	public String getPath() {
		return path;
	}

	public Map<String, String> getQueries() {
		return queries;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}
	
	
	public static Request parse(DataInputStream reader) throws IOException {
		byte delimiter[] = {13, 10};
		String verb = null;
		String path = null;
		Map<String, String> queries = new HashMap<String, String>();
		Map<String, String> headers = new HeaderMap();
		
		// Read HTTP headers and parse out the route.
		String line;
		boolean last = false;
		while(!isEmpty(line = read(reader, delimiter))) {
			// Parse first line
			if(!last) {
				String firstLine[] = line.split("[ ]");
				if(firstLine.length != 3)	// Verb, path and HTTP/1.1
					throwParseError();
				
				// Queries
				int index = firstLine[1].indexOf('?');
				if(index >= 0) {
					path = firstLine[1].substring(0, index);
					String params[] = firstLine[1].substring(index+1, firstLine[1].length()).split("[&]");
					for(String param : params) {
						String value[] = param.split("[=]");
						if(value.length > 2)
							throwParseError();
						else if(value.length != 2)
							queries.put(value[0], null);
						else
							queries.put(value[0], URLDecoder.decode(value[1], "UTF-8"));
					}
				} else
					path = firstLine[1];
				
				verb = firstLine[0];
				
				last = true;
			} else {
				// Parse headers
				int index = line.indexOf(':');
				headers.put(line.substring(0, index), line.substring(index+1, line.length()).trim());
			}
		}
		
		if(path != null)
			path = URLDecoder.decode(path, "utf-8");
		
		return new Request(verb, path, queries, headers);
	}
	
	
	public static Http parseContent(DataInputStream reader, String contentType, Request request, WebServer server) throws IOException {
		// Content-Length
		String _length = request.getHeaders().get("Content-Length");
		int length = 0;
		if(_length != null)
			length = new Integer(_length);
		
		if(contentType.startsWith("application/x-www-form-urlencoded")) {	// Post form
			// Read buffer
			String post = new String(read(reader, length));
			
			// Parse post
			Map<String, Object> forms = new HashMap<String, Object>();
			String pairs[] = post.split("[&]");
			for(String pair : pairs) {
				String vars[] = pair.split("[=]");
				if(vars.length != 2)
					throwParseError();
				forms.put(vars[0], URLDecoder.decode(vars[1], "UTF-8"));
			}
			
			return new Http(request, forms, server);
		} else if(contentType.startsWith("multipart/form-data")) {	// Post form with files
			Map<String, Object> form = new HashMap<String, Object>();
			
			// Boundary
			String boundary = "--" + contentType.substring(contentType.indexOf("boundary")+9, contentType.length()) + "\r\n";
			byte[] byteBoundaryFirst = boundary.getBytes();
			byte[] byteBoundary = ("\r\n" + boundary).getBytes();
			byte[] carry = "\r\n".getBytes();
			
			// Read parts
			String line = "";
			int count = byteBoundaryFirst.length;
			byte[] content;
			
			read(reader, byteBoundaryFirst, length - count); // Quitar primer delimitador;
			count += byteBoundaryFirst.length;
			while(count < length) {
				String name = "";
				String filename = null;
				String encode = "utf-8";
				String mime = "";
				boolean isText = false;
				
				// Headers
				while(true) {
					line = read(reader, carry, length - count);
					count += line.length() + 2;
					if(line.equals(""))
						break;

					// Parse header
					int index = line.indexOf(':');
					String headerName = line.substring(0, index);
					String headerValue = line.substring(index+2, line.length());
					
					// Process header
					String values[];
					switch(headerName) {
						case "Content-Disposition":
							values = headerValue.split("[;]");
							if(values.length != 2 && values.length != 3 && values[0].equals("form-data"))
								throwParseError();
							
							for(int i=1; i<values.length; i++) {
								String vars[] = values[i].split("[=]");
								if(vars.length != 2)
									throwParseError();
								
								switch(vars[0].trim()) {
									case "name":
										name = vars[1].substring(1, vars[1].length()-1);
										break;
										
									case "filename":
										filename = vars[1].substring(1, vars[1].length()-1);
										break;
								}
							}
							
							break;
							
						case "Content-Type":
							mime = headerValue;
							if(headerValue.startsWith("text/")) {
								isText = true;
								encode = charsetFromMime(headerValue);
							}
							break;
					}
				}
				
				// Content
				content = readByte(reader, byteBoundary, length - count);
				count += content.length + boundary.length();
				
				// Put in form
				Object formObject;
				if(isText || filename==null)
					formObject = new String(content, encode);
				else
					formObject = new FormFile(filename, mime, content);
				
				form.put(name, formObject);
			}
			
			return new Http(request, form, server);
		} else if(contentType.startsWith("text/")) {		// Post test
			return new Http(request, new String(read(reader, length), charsetFromMime(contentType)), server);
		} else {		// Post other
			return new Http(request, new FormFile(null, contentType, read(reader, length)), server);
		}
	}

	private static String charsetFromMime(String headerValue) {
		String[] values;
		String encode = null;
		if(headerValue.startsWith("text/")) {
			values = headerValue.split("[;]");
			if(values.length >= 2) {
				for(int i=1; i<values.length; i++) {
					String vars[] = values[i].split("[=]");
					if(vars.length == 2) {
						if(vars[0].trim().equals("charset"))
							encode = vars[1].trim();
					}
				}
			}
			
			if(encode == null)
				encode = "utf-8";
		}
		
		return encode;
	}
	
	private static boolean isEmpty(String str) {
		return str == null || str.equals("");
	}
	
	private static void throwParseError() {
		throw new IllegalArgumentException("Bad request!");
	}
	
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(verb);
		sb.append(" ");
		sb.append(path);
		sb.append(" ");
		sb.append("HTTP/1.1");
		sb.append("\n");
		
		for(Map.Entry<String, String> header : headers.entrySet())
			sb.append(header.getKey() + ": " + header.getValue() + "\n");
		
		sb.append("\n");
		return sb.toString();
	}
	
	
	@SuppressWarnings("serial")
	private static class HeaderMap extends HashMap<String, String> {
		
		@Override
		public String get(Object header) {
			String text = (String) header;
			String res = super.get(text);
			if(res == null)
				res = super.get(text.toLowerCase());
			return res;
		}
		
	}
	
	private static String read(DataInputStream input, byte delimiter[]) throws IOException {
		byte temp[] = new byte[delimiter.length-1];
		int count = 0;
		StringBuilder res = new StringBuilder();
		
		try {
			while(true) {
				byte c = input.readByte();
				
				// Hasta delimitador
				if(delimiter[count] == c) {
					count++;
					if(count == delimiter.length)
						break;
					else
						temp[count-1] = c;
					
					continue;
				} else if(count > 0) {	// Si coincide parte del delimitador, pero no entero
					for(int i=0; i<count; i++)
						res.append((char)temp[i]);
					count = 0;
				}
				
				res.append((char)c);
			}
		} catch(EOFException e) {}
		
		return res.toString();
	}
	
	private static String read(DataInputStream input, byte delimiter[], int limit) throws IOException {
		byte temp[] = new byte[delimiter.length-1];
		int count = 0;
		int forLimit = 0;
		StringBuilder res = new StringBuilder();
		
		try {
			while(true) {
				forLimit++;
				if(forLimit >= limit)
					break;
				
				byte c = input.readByte();
				
				// Hasta delimitador
				if(delimiter[count] == c) {
					count++;
					if(count == delimiter.length)
						break;
					else
						temp[count-1] = c;
					
					continue;
				} else if(count > 0) {	// Si coincide parte del delimitador, pero no entero
					for(int i=0; i<count; i++)
						res.append((char)temp[i]);
					count = 0;
				}
				
				res.append((char)c);
			}
		} catch(EOFException e) {}
		
		return res.toString();
	}
	
	private static byte[] readByte(DataInputStream input, byte delimiter[], int limit) throws IOException {
		byte temp[] = new byte[delimiter.length-1];
		int count = 0;
		int forLimit = 0;
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		
		try {
			while(true) {
				forLimit++;
				if(forLimit >= limit)
					break;
				
				byte c = input.readByte();
				
				// Hasta delimitador
				if(delimiter[count] == c) {
					count++;
					if(count == delimiter.length)
						break;
					else
						temp[count-1] = c;
					
					continue;
				} else if(count > 0) {	// Si coincide parte del delimitador, pero no entero
					for(int i=0; i<count; i++)
						buffer.write(temp[i]);
					count = 0;
				}
				
				buffer.write(c);
			}
		} catch(EOFException e) {}
		
		return buffer.toByteArray();
	}

	private static byte[] read(DataInputStream input, int limit) throws IOException {
		int count = 0;
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		
		try {
			while(true) {
				if(count >= limit)
					break;
				
				byte c = input.readByte();
				buffer.write(c);
				
				count++;
			}
		} catch(EOFException e) {}
		
		return buffer.toByteArray();
	}

}

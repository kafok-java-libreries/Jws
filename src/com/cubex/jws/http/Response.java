package com.cubex.jws.http;

import java.util.HashMap;
import java.util.Map;

public class Response {
	
	private int statusCode;
	private Map<String, String> headers;

	public Response() {
		super();
		headers = new HashMap<String, String>();
		statusCode = 200;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}
	
	
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("HTTP/1.1");
		sb.append(" ");
		sb.append(statusCode);
		sb.append(" ");
		sb.append(StatusCode.valueOf(statusCode).toHumanString());
		sb.append("\n");
		
		for(Map.Entry<String, String> header : headers.entrySet())
			sb.append(header.getKey() + ": " + header.getValue() + "\n");
		
		sb.append("\n");
		return sb.toString();
	}
	
}

package com.cubex.jws.log;

import java.lang.reflect.Method;

import com.cubex.jws.app.Http;
import com.cubex.jws.http.Request;

public interface JwsEvent {
	
	void onException(Throwable e);
	void onWarning(String msg);
	void onRequest(String ip, Request request, Method method);
	void onResponse(String ip, Http http);
	void onStart();
	void onStop();
	
}

package com.cubex.jws.log;

import java.lang.reflect.Method;

import com.cubex.jws.app.Http;
import com.cubex.jws.http.Request;

public abstract class JwsEventAbstract implements JwsEvent {

	public void onException(Throwable e) {}
	public void onWarning(String msg) {}
	public void onRequest(String ip, Request request, Method method) {}
	public void onResponse(String ip, Http http) {}
	public void onStart() {}
	public void onStop() {}

}

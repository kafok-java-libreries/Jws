package com.cubex.jws.log;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class JwsLogger {

	private PrintStream exception;
	private PrintStream warning;
	private PrintStream out;
	private boolean printException;
	private boolean printWarning;
	private boolean printLog;
	private List<JwsEvent> events;
	
	
	public JwsLogger(PrintStream out, PrintStream warning, PrintStream exception) {
		super();
		this.out = out;
		this.warning = warning;
		this.exception = exception;
		
		printException = true;
		printWarning = true;
		printLog = true;
		
		events = new LinkedList<JwsEvent>();
	}


	public JwsLogger(PrintStream out, PrintStream exception) {
		super();
		this.out = out;
		this.warning = out;
		this.exception = exception;
		
		printException = true;
		printWarning = true;
		printLog = true;
		
		events = new LinkedList<JwsEvent>();
	}


	public JwsLogger() {
		super();
		this.out = System.out;
		this.warning = System.out;
		this.exception = System.err;
		
		printException = true;
		printWarning = true;
		printLog = true;
		
		events = new LinkedList<JwsEvent>();
	}
	

	public PrintStream getException() {
		return exception;
	}


	public void setException(PrintStream exception) {
		this.exception = exception;
	}


	public PrintStream getWarning() {
		return warning;
	}


	public void setWarning(PrintStream warning) {
		this.warning = warning;
	}


	public PrintStream getOut() {
		return out;
	}


	public void setOut(PrintStream out) {
		this.out = out;
	}


	public boolean isPrintException() {
		return printException;
	}


	public void setPrintException(boolean printException) {
		this.printException = printException;
	}


	public boolean isPrintWarning() {
		return printWarning;
	}


	public void setPrintWarning(boolean printWarning) {
		this.printWarning = printWarning;
	}


	public boolean isPrintLog() {
		return printLog;
	}


	public void setPrintLog(boolean printLog) {
		this.printLog = printLog;
	}

	public List<JwsEvent> getEvents() {
		return events;
	}
	
	
	public void togglePrint(boolean toggle) {
		printException = toggle;
		printWarning = toggle;
		printLog = toggle;
	}
	
	
	public void logException(Throwable e) {
		this.exception.println("[" + getDate() + "][EXCEPTION]: " + e.getClass().getSimpleName());
		if(printException)
			e.printStackTrace(exception);
		
		for(JwsEvent event : events)
			event.onException(e);
	}
	
	public void logWarning(String warning) {
		if(printWarning)
			this.warning.println("[" + getDate() + "][WARNING]: " + warning);
		
		for(JwsEvent event : events)
			event.onWarning(warning);
	}
	
	public void log(String type, String log) {
		if(printLog)
			this.warning.println("[" + getDate() + "][" + type + "]: " + log);
	}
	
	private String getDate() {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return df.format(new Date(System.currentTimeMillis()));
	}
}

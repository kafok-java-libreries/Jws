package com.cubex.jws.sessions;

import java.util.HashMap;
import java.util.Map;

public class MemorySession implements Session {

	private String id;
	private Map<String, Object> objects;
	private long time;
	
	
	public MemorySession(String id) {
		super();
		this.id = id;
		this.objects = new HashMap<String, Object>();
		time = System.currentTimeMillis();
	}
	

	public String getId() {
		return id;
	}
	
	public Object get(String obj) {
		time = System.currentTimeMillis();
		return objects.get(obj);
	}

	public void put(String obj, Object value) {
		time = System.currentTimeMillis();
		objects.put(obj, value);
	}

	public long getTime() {
		return time;
	}

}

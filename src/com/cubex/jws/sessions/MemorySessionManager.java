package com.cubex.jws.sessions;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MemorySessionManager implements SessionManager {
	
	private static long countId = 0;
	
	private Map<String, MemorySession> sessions;
	private long ttl;
	private Thread thread;
	
	
	public MemorySessionManager(long ttl) {
		sessions = new HashMap<String, MemorySession>();
		this.ttl = ttl;
	}
	

	public Session create() {
		countId++;
		MemorySession session = new MemorySession(""+countId);
		sessions.put(""+countId, session);
		return session;
	}

	public Session find(String id) {
		return sessions.get(id);
	}

	public void destroy(String id) {
		sessions.remove(id);
	}

	public void clear() {
		sessions.clear();
	}

	
	public String getName() {
		return "JWS-SESSIONS";
	}
	
	public void start() {
		thread = new Thread(new SessionCleaner());
		thread.setName("Killer Sessions");
		thread.start();
	}
	
	public void stop() {
		if(thread.isAlive())
			thread.interrupt();
	}
	
	
	private class SessionCleaner implements Runnable {

		public void run() {
			boolean run = true;
			
			while(run) {
				long current = System.currentTimeMillis();
				List<String> list = new LinkedList<String>();
				for(Map.Entry<String, MemorySession> session : sessions.entrySet()) {
					if(session.getValue().getTime()+ttl <= current)
						list.add(session.getValue().getId());
				}
				
				for(String id : list)
					destroy(id);
				
				try {
					Thread.sleep(ttl);
				} catch (InterruptedException e) {
					run = false;
				}
			}
		}
	}

}

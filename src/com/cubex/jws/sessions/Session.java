package com.cubex.jws.sessions;

public interface Session {
	
	String getId();
	Object get(String obj);
	void put(String obj, Object value);
	
}

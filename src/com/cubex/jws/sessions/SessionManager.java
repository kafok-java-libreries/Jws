package com.cubex.jws.sessions;

public interface SessionManager {

	Session create();
	Session find(String id);
	void destroy(String id);
	void clear();
	
	String getName();
	
	void start();
	void stop();
	
}
